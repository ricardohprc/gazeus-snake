package snake.ricardo.com.gazeussnake;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class FinalActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.final_menu);
        Button newGameBtn = findViewById(R.id.newGame);
        Button mainMenuBtn = findViewById(R.id.mainMenu);
        TextView userScore = findViewById(R.id.myScore);
        TextView highScore = findViewById(R.id.highScore);

        userScore.setText(MainActivity.Score + "");
        highScore.setText(MainActivity.HighScore + "");

        newGameBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(FinalActivity.this, MainActivity.class);
                myIntent.setFlags(myIntent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                FinalActivity.this.startActivity(myIntent);
            }
        });

        mainMenuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(FinalActivity.this, FirstActivity.class);
                myIntent.setFlags(myIntent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                FinalActivity.this.startActivity(myIntent);
            }
        });
    }
}
