package snake.ricardo.com.gazeussnake;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import snake.ricardo.com.gazeussnake.Behavior.SnakeBehavior;
import snake.ricardo.com.gazeussnake.Controller.MapViewController;
import snake.ricardo.com.gazeussnake.Helper.GameState;
import snake.ricardo.com.gazeussnake.Helper.MapGenerator;
import snake.ricardo.com.gazeussnake.Helper.OnSwipeTouchListener;
import snake.ricardo.com.gazeussnake.Helper.Vector2;

public class MainActivity extends Activity {

    private MapGenerator mapGenerator;
    private MapViewController mapViewController;
    private GameState gameState;
    private final Handler handler = new Handler();
    private long updateDelay = 120;

    public static int WIDTH = 24;
    public static int HEIGHT = 42;
    public static int Score;
    public static int HighScore;
    public static final String MY_PREFS_NAME = "gazeusSnakeRicardo";
    public static MainActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mapViewController = (MapViewController) findViewById(R.id.mapViewController2);
        gameState = new GameState();
        mapGenerator = new MapGenerator(WIDTH, HEIGHT);

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        HighScore = prefs.getInt("HighScore", 0);
        TextView userScore = findViewById(R.id.inGameScore);
        userScore.setText(Score + "");
        mapViewController.SetGameMap(mapGenerator.GetGeneratedMap());
        instance = this;
        SetGestureListener();
        StartUpdate();
    }

    private void StartUpdate() {
        Score = 0;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(!gameState.hasEnded() ) {
                    mapViewController.update();
                    handler.postDelayed(this, updateDelay);
                } else {
                    if(Score > HighScore){
                        HighScore = Score;
                    }
                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putInt("HighScore", HighScore);
                    editor.apply();
                    Intent myIntent = new Intent(MainActivity.this, FinalActivity.class);
                    myIntent.setFlags(myIntent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                    MainActivity.this.startActivity(myIntent);
                }
            }
        }, updateDelay);
    }

    public void AddPoint(){
        TextView userScore = findViewById(R.id.inGameScore);
        userScore.setText(Score + "");
        Score++;
        updateDelay = 120 - Math.min(30, Score*2);
    }

    private void SetGestureListener(){
        mapViewController.setOnTouchListener(new OnSwipeTouchListener(MainActivity.this) {
            public void onSwipeTop() {
                SnakeBehavior.instance.tryToSetDirection(new Vector2(0,-1));
            }
            public void onSwipeRight() {
                SnakeBehavior.instance.tryToSetDirection(new Vector2(1,0));
            }
            public void onSwipeLeft() {
                SnakeBehavior.instance.tryToSetDirection(new Vector2(-1,0));
            }
            public void onSwipeBottom() {
                SnakeBehavior.instance.tryToSetDirection(new Vector2(0,1));
            }

        });
    }
}
