package snake.ricardo.com.gazeussnake.Helper;

import android.graphics.Color;
import android.os.Build;
import android.service.quicksettings.Tile;
import android.support.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import snake.ricardo.com.gazeussnake.Behavior.SnakeBehavior;

public class MapGenerator {

    private TileType[][] MapTiles;
    private SnakeBehavior snakeBehavior;
    private Vector2 FruitPosition;
    public static int WIDTH;
    public static int HEIGHT;
    public static MapGenerator instance;

    public MapGenerator(int width, int height) {
        instance = this;
        WIDTH = width;
        HEIGHT = height;
        snakeBehavior = new SnakeBehavior();
        InitialGameState();

    }

    public void InitialGameState(){
        MapTiles = new TileType[WIDTH][HEIGHT];
        FruitPosition = new Vector2(WIDTH / 4, HEIGHT / 2);
        UpdateTiles();
    }

    public void AddFuitOnRandomPosition() {
        Random rng = new Random();
        int x = rng.nextInt(WIDTH);
        int y = rng.nextInt(HEIGHT);
        if(GetTileType(x, y) == TileType.Empty && !TooCloseToBorder(x,y)){
            //Evitando as bordas pois como o jogo funciona na base do Swipe, fica complicado ter uma precisao nas bordas
            FruitPosition = new Vector2(x, y);
        } else {
            AddFuitOnRandomPosition();
        }
    }

    public boolean TooCloseToBorder(int x, int y){
        return x == 1 || y == 1 || x == WIDTH - 1 || y == HEIGHT - 1;
    }

    public TileType[][] GetGeneratedMap(){
        return MapTiles;
    }

    public TileType[][] UpdateTiles(){
        for(int i = 0; i < WIDTH; i++){
            for(int j = 0; j < HEIGHT; j++){
                MapTiles[i][j] = GetTileType(i, j);
            }
        }
        return MapTiles;
    }

    private TileType GetTileType(int i, int j){
        if (isOnTheEdge(i, j)) {
            return TileType.Wall;
        } else if (snakeBehavior.isSnake(i,j)) {
            return TileType.Snake;
        } else if (snakeBehavior.isSnakeTail(i,j)) {
            return TileType.SnakeTail;
        } else if (FruitPosition.equals(i, j)) {
            return TileType.Fruit;
        } else {
            return TileType.Empty;
        }
    }

    public boolean isOnTheEdge(int i, int j){
        return  (i == 0 || j == 0 || i == WIDTH - 1 || j == HEIGHT - 1);
    }

    public boolean isOnTheFruit(int i, int j){
        return (i == FruitPosition.x && j == FruitPosition.y);
    }

    public enum TileType {
        Empty(0, Color.TRANSPARENT),
        Wall(1, Color.BLACK),
        Fruit(2, Color.RED),
        Snake(3, Color.BLUE),
        SnakeTail(4, 0x550000ff);

        private int value;
        private int color;

        TileType(int value, int color) {
            this.value = value;
            this.color = color;
        }

        public int getValue() { return value; }
        public int getColor() { return color; }
        public int getColorByDistance(int i, int j) {
            //https://developer.android.com/reference/android/graphics/Color
            double distance = Vector2.Distance(SnakeBehavior.instance.GetHeadPosition(), new Vector2(i, j));
            int alpha = (int) (100 * ( 1 - Math.min(distance / Vector2.MAXDISTANCE, 1)));
            return (Math.max(alpha, 20) & 0xff) << 24 | (0 & 0xff) << 16 | (0 & 0xff) << 8 | (0 & 0xff);
        }
    }
}
