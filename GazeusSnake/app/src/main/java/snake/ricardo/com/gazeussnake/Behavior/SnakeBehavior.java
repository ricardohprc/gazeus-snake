package snake.ricardo.com.gazeussnake.Behavior;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import snake.ricardo.com.gazeussnake.Helper.MapGenerator;
import snake.ricardo.com.gazeussnake.Helper.Vector2;
import snake.ricardo.com.gazeussnake.MainActivity;

public class SnakeBehavior {

    private List<Vector2> Snake;
    public static SnakeBehavior instance;
    private Vector2 Direction;

    public SnakeBehavior(){
        instance = this;
        Snake = new ArrayList<>();
        SnakeInitialPosition();
    }

    public Vector2 GetHeadPosition(){
        return Snake.get(0);
    }

    public boolean tryToSetDirection(Vector2 direction){
        if(!Direction.add(direction).equals(Vector2.zero)){
            Direction = direction;
            return true;
        }
        return false;
    }

    public Vector2 MoveSnake(){
        Vector2 head = Snake.get(0);
        Snake.add(0, head.add(Direction));
        Snake.remove(Snake.size() - 1);
        return Snake.get(0);
    }

    public void FuitEaten(){
        Snake.add(GetHeadPosition());
    }

    private void SnakeInitialPosition(){
        Direction = new Vector2(-1, 0);
        Snake.add(new Vector2(MapGenerator.WIDTH/2, MapGenerator.HEIGHT/2));
        Snake.add(new Vector2(MapGenerator.WIDTH/2 +1, MapGenerator.HEIGHT/2));
        Snake.add(new Vector2(MapGenerator.WIDTH/2 +2, MapGenerator.HEIGHT/2));
    }

    public boolean isSnake(int i, int j){
        return (Snake.get(0).x == i && Snake.get(0).y == j);
    }

    public boolean isSnakeTail(int i, int j){
        for (Vector2 snake: Snake) {
            if(snake.x == i && snake.y == j && !snake.equals(Snake.get(0))){
                return true;
            }
        }
        return false;
    }
}
