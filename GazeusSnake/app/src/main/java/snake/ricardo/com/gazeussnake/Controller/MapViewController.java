package snake.ricardo.com.gazeussnake.Controller;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;


import snake.ricardo.com.gazeussnake.Behavior.SnakeBehavior;
import snake.ricardo.com.gazeussnake.Helper.GameState;
import snake.ricardo.com.gazeussnake.Helper.MapGenerator;
import snake.ricardo.com.gazeussnake.Helper.Vector2;
import snake.ricardo.com.gazeussnake.MainActivity;

public class MapViewController extends View {

    private Paint TilePainter = new Paint();
    private MapGenerator.TileType[][] GameMap;

    public MapViewController(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void SetGameMap(MapGenerator.TileType[][] gameMap){
        GameMap = gameMap;
    }


    public void update() {
        CheckColision();
        SnakeBehavior.instance.MoveSnake();
        SetGameMap(MapGenerator.instance.UpdateTiles());
        reDraw();
    }

    private void CheckColision() {
        Vector2 snakeHead = SnakeBehavior.instance.GetHeadPosition();
        if(MapGenerator.instance.isOnTheEdge(snakeHead.x , snakeHead.y) || SnakeBehavior.instance.isSnakeTail(snakeHead.x, snakeHead.y)){
            GameState.instance.SetEndState();
        } else if(MapGenerator.instance.isOnTheFruit(snakeHead.x, snakeHead.y)){
            SnakeBehavior.instance.FuitEaten();
            MapGenerator.instance.AddFuitOnRandomPosition();
            MainActivity.instance.AddPoint();
        }
    }

    protected void reDraw() {
        this.invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);
        if(GameMap != null){
            Vector2 mapSize = new Vector2(canvas.getWidth() / GameMap.length, canvas.getHeight() / GameMap[0].length);
            float size = Math.min(mapSize.x, mapSize.y) / 2f;

            for (int i = 0; i < GameMap.length; i++){
                for(int j = 0; j < GameMap[0].length;j++){
                    int rx = i * mapSize.x;
                    int ry = j * mapSize.y;

                    if(MapGenerator.instance.isOnTheEdge(i, j)){
                        TilePainter.setColor(GameMap[i][j].getColorByDistance(i,j));
                        canvas.drawRect(new Rect(rx,ry,rx + (int)size * 2,ry + (int)size * 2), TilePainter);
                    } else {
                        TilePainter.setColor(GameMap[i][j].getColor());
                        canvas.drawRect(new Rect(rx,ry,rx + (int)size * 2,ry + (int)size * 2), TilePainter);
                    }
                }
            }
        }

    }

}
