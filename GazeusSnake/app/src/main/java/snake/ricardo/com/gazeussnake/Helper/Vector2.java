package snake.ricardo.com.gazeussnake.Helper;

import snake.ricardo.com.gazeussnake.MainActivity;

public class Vector2 {

    public int x;
    public int y;
    public static Vector2 zero = new Vector2(0,0);
    public static double MAXDISTANCE = Distance(new Vector2(0,0), new Vector2(MainActivity.WIDTH/2 ,MainActivity.HEIGHT/2));

    public Vector2(int x, int y){
        this.x = x;
        this.y = y;
    }

    public boolean equals(int x, int y){
        return (this.x == x && this.y == y);
    }

    public boolean equals(Vector2 vector){
        return (this.x == vector.x && this.y == vector.y);
    }

    public Vector2 add(Vector2 vector){
        return new Vector2(this.x + vector.x, this.y + vector.y);
    }

    public static double Distance(Vector2 a, Vector2 b){
        return Math.hypot(a.x - b.x, a.y - b.y);
    }

}
