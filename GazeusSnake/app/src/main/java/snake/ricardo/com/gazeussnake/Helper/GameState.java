package snake.ricardo.com.gazeussnake.Helper;

public class GameState {

    public enum State{ Started, Transition, Playing, Done };
    private static State CurrentState;
    public static GameState instance;

    public GameState(){
        Init();
        instance = this;
    }

    public void Init(){
        CurrentState = State.Started;
    }

    public void SetTransitionState(){
        CurrentState = State.Transition;
    }

    public void SetPlayingState(){
        CurrentState = State.Playing;
    }

    public void SetEndState(){
        CurrentState = State.Done;
    }

    public void SetStartedState(){
        CurrentState = State.Started;
    }

    public boolean isPlaying(){
        return CurrentState == State.Playing;
    }

    public boolean hasEnded(){
        return CurrentState == State.Done;
    }
}
